const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: {
        client: './src/client/main.js',
    },
    output: {
        filename: "bundle.js",
    },
    plugins: [
        new CopyPlugin([
            { from: 'src/client/index.html', to: '.' },
            { from: 'src/client/main.css', to: '.' },
        ]),
    ],
}