// components
import Card from './components/Card.js';
import Cart from './components/Cart.js';
import ItemList from './components/ItemList.js';
import Popup from './components/Popup.js';
import { CartItemList } from './components/CartItemLIst.js';
import { FrontItemList } from './components/FrontItemLIst.js';
// main shop
import Shop from './Shop.js';

window.toggleCart = () => {
    const main = document.querySelector('main');
    if (main.hasAttribute('cart')) {
        main.removeAttribute('cart');
    } else {
        main.setAttribute('cart', '');
    }
}

window.formatPrice = (number) => {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number);
}
