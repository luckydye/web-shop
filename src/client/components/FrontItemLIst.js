import ItemList from './ItemList';
import { html } from 'lit-html';
import Shop from '../Shop';

export class FrontItemList extends ItemList {

    static templateStyle() {
        return `
            .card-list {
                display: grid;
                grid-template-columns: 1fr 1fr 1fr;
                grid-gap: 15px;
            }

            .card-list ws-card {
                border: 1px solid #333;
                border-radius: 5px;
            }
            
            @media screen and (max-width: 1200px) {
                .card-list {
                    grid-template-columns: 1fr 1fr;
                }
            }

            @media screen and (max-width: 900px) {
                .card-list {
                    grid-template-columns: 1fr;
                }
            }

            .price {
                font-size: 18px;
                margin-top: 10px;
            }
        `;
    }

    static itemTemplate(item) {
        return html`
            <ws-card>
                <span slot="title">${item.name}</span>
                <span slot="description">${item.description}</span>
                <span slot="footer" class="price">${formatPrice(item.price)}</span>
                <button slot="footer" @click=${() => Shop.addToCart(item)}>Add to cart</button>
            </ws-card>
        `;
    }

}

customElements.define('ws-front-item-list', FrontItemList);