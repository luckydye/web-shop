import { html, render } from 'lit-html';
import Shop from '../Shop';

export default class ShopComponent extends HTMLElement {

    static template(props) {
        return html``;
    }

    constructor() {
        super();

        this.props = {};

        this.attachShadow({ mode: 'open' });
    }

    onStateChange() {

    }

    connectedCallback() {
        this.render();

        Shop.store.subscribe(() => {
            this.onStateChange();
        });
    }

    render() {
        render(this.constructor.template(this.props), this.shadowRoot);
    }

}
