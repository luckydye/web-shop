import { html, render } from 'lit-html';
import ShopComponent from './ShopComponent.js';

export default class Popup extends ShopComponent {

    static template(props) {
        return html`
            <style>
                :host {
                    position: fixed;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background: rgba(0, 0, 0, 0.15);
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }

                .popup-wrapper {
                    background: white;
                    border-radius: 10px;
                    max-width: 900px;
                    padding: 15px 25px;
                    box-shadow: 2px 4px 10px rgba(0, 0, 0, 0.125);
                    position: relative;
                    z-index: 1000;
                    max-height: 90vh;
                    overflow: auto;
                    min-width: 300px;
                }
                
                canvas {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    display: block;
                    pointer-events: none;
                }
            </style>
            <canvas></canvas>
            <div class="popup-wrapper">
                <slot></slot>
            </div>
        `;
    }

    connectedCallback() {
        super.connectedCallback();

        const canvas = this.shadowRoot.querySelector('canvas');
        const ctx = canvas.getContext("2d");

        canvas.width = canvas.offsetWidth;
        canvas.height = canvas.offsetHeight;

        const particles = [];

        const spawn = (amount) => {
            for(let i = 0; i < amount; i++) {
                particles.push([
                    40, 
                    50, 
                    Math.random() * Math.PI * 2 + (2 * Math.random()),
                    10 * Math.random() + 4,
                    `hsl(${Math.floor(360 * Math.random())}, 100%, 75%)`,
                ]);
            }
        }

        const updaet = () => {
            for(let p of [...particles]) {
                const dir = p[2];
                const vel = p[3];
                p[3] *= 0.95;
                p[0] += Math.sin(dir) * vel - 1;
                p[1] += Math.cos(dir) * vel + 2;

                if(p[3] < 0.75 * Math.random()) {
                    particles.splice(particles.indexOf(p), 1);
                }
            }
        }

        const render = () => {

            updaet();

            ctx.clearRect(0, 0, canvas.width, canvas.height);

            for(let [x, y, _1, _2, color] of particles) {
                ctx.fillStyle = color;
                ctx.beginPath();
                ctx.arc(canvas.width / 2 + x, canvas.height / 3 + y, 2, 0, 2 * Math.PI);
                ctx.fill();
            }
            
            if(particles.length > 0) {
                requestAnimationFrame(render);
            }
        }

        spawn(500);
        render();
    }

}

customElements.define('ws-popup', Popup);
