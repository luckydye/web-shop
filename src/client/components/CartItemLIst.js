import ItemList from './ItemList';
import { html } from 'lit-html';
import Shop from '../Shop';

export class CartItemList extends ItemList {

    static itemTemplate(item) {
        const state = Shop.store.getState().cart;
        return html`
            <style>
                input {
                    float: right;
                    width: 40px;
                    text-align: right;
                }
                .button {
                    float: right;
                    cursor: pointer;
                    padding: 0 5px;
                    border: 1px solid transparent;
                    border-radius: 3px;
                    transition: border .1s ease-out;
                }
                .button:hover {
                    border: 1px solid #333;
                    box-shadow: 1px 2px 8px rgba(0, 0, 0, 0.1);
                }
                .button:active {
                    background: rgba(0, 0, 0, 0.1);
                    box-shadow: 1px 2px 0px rgba(0, 0, 0, 0.1);
                }
                ws-card {
                    --card-padding: 15px 0;
                }
                .price {
                    flex: 1;
                }
                svg {
                    display: block;
                }
            </style>
            <ws-card>
                <span slot="title">${item.name}</span>
                <span slot="footer" class="price">${formatPrice(item.price)}</span>
                <input slot="footer" type="number" value="${state[item.id]}" @input=${(e) => this.onAmountChange(item, e.target)}>Add to cart</input>
                <a class="button" slot="footer" @click=${(e) => Shop.removeFromCart(item)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="25" viewBox="0 0 24 24"><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                </a>
            </ws-card>
        `;
    }

    static onAmountChange(item, input) {
        Shop.setCartAmount(item, input.value);
    }

    get itemFilter() {
        const state = Shop.store.getState();
        return { id: [...new Set(Object.keys(state.cart))] };
    }

}

customElements.define('ws-cart-item-list', CartItemList);