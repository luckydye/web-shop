import { html, render } from 'lit-html';

export default class Card extends HTMLElement {

    static template(props) {
        return html`
            <style>
                :host {
                    --background: transparent;
                    --card-padding: 15px;
                }

                .card-wrapper {
                    background: var(--background);
                    border-radius: 3px;
                    padding: var(--card-padding);
                    display: flex;
                    flex-direction: column;
                    height: 100%;
                    box-sizing: border-box;
                }

                .title {
                    font-size: 18px;
                    font-weight: 100;
                    margin-bottom: 8px;
                }

                .description {
                    font-size: 16px;
                    flex: 1;
                }

                .details {
                    margin-top: 15px;
                    display: flex;
                    justify-content: space-between;
                }
            </style>
            <div class="card-wrapper">
                <div class="title">
                    <slot name="title"></slot>
                </div>
                <div class="description">
                    <slot name="description"></slot>
                </div>
                <div class="details">
                    <slot name="footer"></slot>
                </div>
            </div>
        `;
    }

    constructor({
        title = "Title",
        description = "Description",
    } = {}) {
        super();

        this.props = {
            title,
            description,
        };

        this.attachShadow({ mode: 'open' });
    }

    connectedCallback() {
        render(Card.template(this.props), this.shadowRoot);
    }

}

customElements.define('ws-card', Card);
