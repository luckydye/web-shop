import { html, render } from 'lit-html';
import ShopComponent from './ShopComponent';
import Shop from '../Shop';

export default class ItemList extends ShopComponent {

    static templateStyle() {
        return ``;
    }

    static itemTemplate(item) {
        return html`
            <ws-card>
                <span slot="title">${item.title}</span>
                <span slot="description">${item.description}</span>
                <span slot="footer" class="price">€ ${item.price}</span>
            </ws-card>
        `;
    }

    static template({ items }) {
        return html`
            <link rel="stylesheet" href="./main.css">
            <style>
                ${this.templateStyle()}
            </style>
            <div class="card-list">
                ${[...items].map(item => this.itemTemplate(item))}
            </div>
        `;
    }

    get itemFilter() {
        return {};
    }

    get items() {
        return this.props.items;
    }

    constructor() {
        super();

        this.props = {
            items: []
        }

        this.update();
    }

    update() {
        return Shop.requestItems(this.itemFilter).then(json => {
            this.props.items = json;
            this.render();
            return json;
        })
    }
}

customElements.define('ws-item-list', ItemList);