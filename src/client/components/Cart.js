import { html, render } from 'lit-html';
import ShopComponent from './ShopComponent.js';
import Shop from '../Shop.js';
import { CartItemList } from './CartItemLIst.js';

const cartItemList = new CartItemList();

export default class Cart extends ShopComponent {

    static template(cart) {
        return html`
            <link rel="stylesheet" href="./main.css">
            <style>
                :host {
                    position: fixed;
                    top: 0;
                    right: 0;
                    height: 100%;
                    width: 250px;
                    padding: 15px 20px;
                    box-sizing: border-box;
                    border-left: 1px solid #333;
                }

                .cart {
                    display: flex;
                    flex-direction: column;
                    justify-content: space-between;
                    height: 100%;
                }

                .item-list {
                    overflow: auto;
                    position: relative;
                    height: 100%;
                }

                .cart-header {
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                }

                .close-cart {
                    cursor: pointer;
                }

                ws-card {
                    border: none;
                }

                .saldo {
                    margin-bottom: 20px;
                }

                h3 {
                    margin: 0;
                    display: inline;
                }
            </style>
            <div class="cart">
                <div class="cart-header">
                    <div class="cart-icon" data-item-count="0" onclick="toggleCart()">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 2v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.14 0-.25-.11-.25-.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.08-.14.12-.31.12-.48 0-.55-.45-1-1-1H5.21l-.94-2H1zm16 16c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z" />
                            <path d="M0 0h24v24H0z" fill="none" />
                        </svg>
                        <h3>Cart</h3>
                    </div>    
                </div>
                <div class="item-list">
                    ${cartItemList}
                </div>
                <div class="saldo">
                    <a>Final price:</a>
                    <a>${formatPrice(this.getCartPrice())}</a>
                </div>
                <button @click="${() => Shop.checkout()}">Checkout</button>
            </div>
        `;
    }

    static getCartPrice() {
        const cart = Shop.store.getState().cart;
        const items = cartItemList.items;

        let price = 0;

        for(let id in cart) {
            const item = items.find(item => item.id == id);
            if(item) {
                const amount = cart[id];
                price += item.price * amount;
            }
        }
        
        return price;
    }

    constructor() {
        super();

        this.onStateChange();
    }

    onStateChange() {
        cartItemList.update().then(items => {
            this.render();
        });
    }

}

customElements.define('ws-cart', Cart);
