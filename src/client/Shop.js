import { createStore } from 'redux';
import { html, render } from 'lit-html';

function getInitialLocalState() {
    const storage = localStorage.getItem('ws-state');
    if(storage) {
        try {
            return JSON.parse(storage);
        } catch(err) {}
    }
    
    return {
        cart: {}
    };
}

function saveLocalState(state) {
    localStorage.setItem('ws-state', JSON.stringify(state));
}

const initialState = getInitialLocalState();

function webShopReducer(state = initialState, action) {
    const type = action.type;
    const data = action.data;
    const cart = state.cart;

    switch (type) {
        case 'cart.add':
            state.cart[data.id] = state.cart[data.id] ? state.cart[data.id] + 1 : 1;
            break;

        case 'cart.set':
            state.cart[data.id] = data.amount;
            break;

        case 'cart.delete':
            if (state.cart[data.id]) {
                delete state.cart[data.id];
            }
            break;

        case 'cart.clear':
            state.cart = {};
            break;
    }

    saveLocalState(state);

    return {
        cart
    };
}

const store = createStore(webShopReducer);

export default class Shop {

    static get store() {
        return store;
    }

    static checkout() {
        return fetch('/api/checkout', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(Shop.store.getState().cart)
        }).then(res => {
            res.json().then(data => {
                Shop.store.dispatch({ type: "cart.clear" });
                const popup = document.createElement('ws-popup');
                render(html`
                    <h3>Your numbers:</h3>
                    <style>
                        .item {
                            padding: 10px 0;
                            text-align: center;
                        }
                    </style>
                    ${data.map(item => {
                        return html`
                            <div class="item">
                                <div>${item.name}</div>
                                <div>${item.secret}</div>
                            </div>
                        `;
                    })}
                `, popup);
                document.body.appendChild(popup);
            })
        })
    }

    static addToCart(item) {
        this.store.dispatch({ type: 'cart.add', data: item });
    }

    static removeFromCart(item) {
        this.store.dispatch({ type: 'cart.delete', data: item });
    }

    static setCartAmount(item, amount) {
        this.store.dispatch({ type: 'cart.set', data: {
            id: item.id,
            amount: +amount
        }});
    }

    static requestItems(filter) {
        const filterKeys = [];

        for (let key in filter) {
            filterKeys.push(key + "=" + filter[key]);
        }

        const query = "?" + filterKeys.join("&");
        const url = "/api/items" + query;

        return fetch(url).then(res => res.json().then(json => {
            return json;
        }))
    }

}
