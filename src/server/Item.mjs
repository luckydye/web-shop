
function random() {
    return `${Math.floor(Math.random() * 398536 * Date.now())}`;
}

export default class Item {

    constructor({ name = "unknown", id, description, price = 0 }) {
        this.id = "" + id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    get secret() {
        return random();
    }

}