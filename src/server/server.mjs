import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import WebShop from './WebShop.mjs';
import Item from './Item.mjs';

const app = express();
const PORT = process.env.PORT || 3000;

const shop = new WebShop();

import items from './items.json';

for(let item of items) {
    shop.addItem(new Item(item));
}

app.use('/', express.static(path.resolve('./dist')));

app.get('/api/items', (req, res) => {
    const filter = {};

    for (let key in req.query) {
        filter[key] = req.query[key] ? req.query[key].split(",") : [];
    }

    const result = shop.findItems(filter);
    res.send(result);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.post('/api/checkout', (req, res) => {
    const cart = req.body;
    const items = shop.findItems({ id: Object.keys(req.body) });
    const result = items.map(item => {
        const items = [];
        for(let i = 0; i < cart[item.id]; i++) {
            items.push({
                id: item.id,
                name: item.name,
                secret: item.secret
            });
        }
        return items;
    })
    res.send(result.flat());
});

app.listen(PORT, () => console.log(`App listening on port ${PORT}!`));
