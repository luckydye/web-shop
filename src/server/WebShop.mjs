import Item from './Item.mjs';

export default class WebShop {

    items = new Set();

    addItem(item) {
        this.items.add(item);
    }

    deleteItem(item) {
        this.items.delete(item);
    }

    findItems(params) {
        const result = [];

        const keys = Object.keys(params);

        if (keys.length > 0) {
            for (let item of this.items)
                for (let key of keys)
                    for (let value of params[key]) {
                        if (String(item[key]).match(value)) {
                            result.push(item);
                        }
                    }
        } else {
            result.push(...[...this.items].map(item => ({
                id: item.id,
                name: item.name,
                description: item.description,
                price: item.price,
            })).slice(0, 100));
        }

        return result;
    }

}
